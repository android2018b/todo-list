package com.example.sebastian.todo_list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ListDetailActivity : AppCompatActivity() {

  val database = FirebaseDatabase.getInstance()
  val ref = database.getReference("todo-list")

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_list_detail)

    val listId = intent.getStringExtra(ActivityList.INTENT_LIST_ID)

    ref.child(listId).child("list-name")
            .addListenerForSingleValueEvent(object : ValueEventListener{
              override fun onCancelled(p0: DatabaseError) {
              }

              override fun onDataChange(dataSnapshot: DataSnapshot) {
                title = dataSnapshot.value.toString()
              }
            })
  }
}
